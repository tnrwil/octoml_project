provider "google" {
  credentials = file("/builds/tnrwil/octoml_project/account.json")
  project     = "challenge-treed"
  region      = "us-west1"
}




resource "google_container_cluster" "primary-west" {
  name               = "treed-takehome-west"
  location           = "us-west1"
  node_locations     = ["us-west1-a", "us-west1-b"]
  description        = "take-home-challenge"
  initial_node_count = 3
  cluster_autoscaling {
    enabled = true
    resource_limits {
      resource_type = "cpu"
      maximum       = 100
    }
    resource_limits {
      resource_type = "memory"
      maximum       = 96
    }
  }

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = true
    }
  }

  node_config {

    disk_size_gb = "15"
    disk_type    = "pd-ssd"
    machine_type = "n1-standard-1"
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels = {
      takehome = "challenge"
    }

    tags = ["takehome", "challenge", "us-west1"]
  }

  addons_config {
    http_load_balancing {
      disabled = false
    }

    horizontal_pod_autoscaling {
      disabled = false
    }
  }

  timeouts {
    create = "30m"
    update = "40m"
  }
}
