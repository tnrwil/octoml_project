output "ip" {
  value = google_container_cluster.primary-west.endpoint
}

output "certificate" {
  value = google_container_cluster.primary-west.master_auth.0.client_certificate
}


output "client_key" {
  value = google_container_cluster.primary-west.master_auth.0.client_key
}

output "root_cert" {
  value = google_container_cluster.primary-west.master_auth.0.cluster_ca_certificate
}
