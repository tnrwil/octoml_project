provider "google" {
  credentials = file("/builds/tnrwil/octoml_project/account.json")
  project     = "challenge-treed"
  region      = "us-central1"
}




resource "google_container_cluster" "primary" {
  name               = "treed-takehome-central"
  location           = "us-central1"
  node_locations     = ["us-central1-c", "us-central1-f"]
  description        = "take-home-challenge"
  initial_node_count = 3
  cluster_autoscaling {
    enabled = true
    resource_limits {
      resource_type = "cpu"
      maximum       = 100
    }
    resource_limits {
      resource_type = "memory"
      maximum       = 96
    }
  }

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = true
    }
  }

  node_config {

    disk_size_gb = "15"
    disk_type    = "pd-ssd"
    machine_type = "n1-standard-1"
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels = {
      takehome = "challenge"
    }

    tags = ["takehome", "challenge", "us-central1"]
  }

  addons_config {
    http_load_balancing {
      disabled = false
    }

    horizontal_pod_autoscaling {
      disabled = false
    }
  }

  timeouts {
    create = "30m"
    update = "40m"
  }
}
