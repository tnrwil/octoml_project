resource "random_id" "random_number" {
  byte_length = 4
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_%@"
}

resource "google_sql_database_instance" "master" {
  name             = "master-${random_id.random_number.hex}"
  region           = "us-central-1"
  database_version = "POSTGRES_12"
  project          = var.project_id

  settings {
    tier              = "db-f1-micro"
    availability_type = "REGIONAL"
    disk_size         = "100"
    backup_configuration {
      enabled = true
    }
    ip_configuration {
      ipv4_enabled    = true
      private_network = "projects/${var.project_id}/global/networks/default"
    }
    location_preference {
      zone = "us-central-1-a"
    }
  }
  timeouts {
    create = "60m"
    update = "60m"
  }

}

resource "google_sql_user" "main" {
  depends_on = [
    google_sql_database_instance.master
  ]
  name     = "main"
  instance = google_sql_database_instance.master.name
  password = random_password.password.result 
}

resource "google_sql_database" "main" {
  depends_on = [
    google_sql_user.main
  ]
  name     = "main"
  instance = google_sql_database_instance.master.name
}


resource "google_sql_database_instance" "read_replica" {
  name                 = "replica-${random_id.random_number.hex}"
  master_instance_name = "${var.project_id}:${google_sql_database_instance.master.name}"
  region               = "us-east1"
  database_version     = "POSTGRES_12"
  project              = var.project_id

  replica_configuration {
    failover_target = false
  }

  settings {
    tier              = "db-f1-micro"
    availability_type = "ZONAL"
    disk_size         = "100"
    backup_configuration {
      enabled = false
    }
    ip_configuration {
      ipv4_enabled    = true
      private_network = "projects/${var.project_id}/global/networks/default"
    }
    location_preference {
      zone = "us-east-1-b"
    }
  }
  timeouts {
    create = "60m"
    update = "60m"
  }

}
