output "ip" {
   value = google_sql_database_instance.master.ip_address.0.ip_address
}


output "public_ip" {
   value = google_sql_database_instance.master.public_ip_address
}


output "link" {
   value = google_sql_database_instance.master.self_link
}
