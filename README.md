DIAGRAM
=======
The diagram of the work flow of this project is hosted here and labeled https://gitlab.com/tnrwil/octoml_project challenge-treed.PNG


SECURITY
=========
Added security would be to store the json file in vault. The json file is stored in gitlab.

* The database should be stored on a private network.
* The front end should only be able to connect to the application
* Consul could be used in this case with intentions so that you can further lock it down
* The cluster should be locked down by namespaces for the application deployment



BUILD APPLICATION
==================
The application the is built in separate repository. The name of the repo is docker_build. This will build the container and push to the gitlab's registry for the that same repo. 

```
The application is located here 
registry.gitlab.com/tnrwil/docker_build:takehomev1
```


```
You can trigger a build by changing the TAGS from takehomev6 to takehomev7 in.gitlab-ci.yml file.
This will then build the application, push to docker registry in gitlab, build cluster, and deploy application, create databse and read replicas.

```

```
Here is the location of the application build that will trigger the build of the clusters
https://gitlab.com/tnrwil/docker_build.git
```

BUILD CLUSTERS
==============
Terraform wil build three clusters. Three regions

* us-central1
* us-east1
* us-west1

```
[root@b7e118e9200e /]# gcloud container clusters list
NAME                    LOCATION     MASTER_VERSION  MASTER_IP      MACHINE_TYPE   NODE_VERSION    NUM_NODES  STATUS
treed-takehome-central  us-central1  1.14.10-gke.36  34.68.1.189    n1-standard-1  1.14.10-gke.36  6          RUNNING
treed-takehome-east     us-east1     1.14.10-gke.36  34.75.61.3     n1-standard-1  1.14.10-gke.36  6          RUNNING
treed-takehome-west     us-west1     1.14.10-gke.36  34.82.200.252  n1-standard-1  1.14.10-gke.36  6          RUNNING
```

DEPLOY APPLICATION
==================
In order to deploy the application I used gcloud to create the kubeconfig file. Then use kubectl deploy the application as a Deployment of three nodes. An example of this is below

```
$ gcloud container clusters get-credentials treed-takehome-central  --zone=us-central1
 Fetching cluster endpoint and auth data.
 kubeconfig entry generated for treed-takehome-central.
 $ kubectl apply -f takehome.yaml
 deployment.apps/takehome-challenge created
 $ kubectl get pods
 NAME                                  READY   STATUS              RESTARTS   AGE
 takehome-challenge-5c9c84ffd9-562ht   0/1     ContainerCreating   0          0s
 takehome-challenge-5c9c84ffd9-8g4ns   0/1     ContainerCreating   0          0s
 takehome-challenge-5c9c84ffd9-ktqll   0/1     ContainerCreating   0          0s
 $ gcloud container clusters get-credentials treed-takehome-east  --zone=us-east1
 Fetching cluster endpoint and auth data.
 kubeconfig entry generated for treed-takehome-east.
```



DEPLOY DATABASES
==================

I used terraform to create the database and it seems it timed out but was created. If you look in the database folder. You will see  the main.tf and output.tf file
It seems the databases were created. I went back and created more databases. Below is an example of that work. 


```
[root@b7e118e9200e /]# gcloud sql instances create treed --region=us-west1
ERROR: (gcloud.sql.instances.create) Resource in project [challenge-treed] is the subject of a conflict: The Cloud SQL instance already exists. When you delete an instance, you cannot reuse the name of the deleted instance until one week from the deletion date.
```

```
[root@b7e118e9200e /]# gcloud sql instances create treed --region=us-east1
ERROR: (gcloud.sql.instances.create) Resource in project [challenge-treed] is the subject of a conflict: The Cloud SQL instance already exists. When you delete an instance, you cannot reuse the name of the deleted instance until one week from the deletion date.
```
```
[root@b7e118e9200e /]# gcloud sql instances create treed-1 --region=us-east1
Creating Cloud SQL instance...done.                                                                                       
Created [https://sqladmin.googleapis.com/sql/v1beta4/projects/challenge-treed/instances/treed-1].
NAME     DATABASE_VERSION  LOCATION    TIER              PRIMARY_ADDRESS  PRIVATE_ADDRESS  STATUS
treed-1  MYSQL_5_7         us-east1-b  db-n1-standard-1  34.75.50.152     -                RUNNABLE
```
```
[root@b7e118e9200e /]# gcloud sql instances list
NAME     DATABASE_VERSION  LOCATION       TIER              PRIMARY_ADDRESS  PRIVATE_ADDRESS  STATUS
treed    MYSQL_5_7         us-central1-a  db-n1-standard-1  34.69.232.82     -                RUNNABLE
treed-1  MYSQL_5_7         us-east1-b     db-n1-standard-1  34.75.50.152     -                RUNNABLE
[root@b7e118e9200e /]# gcloud sql instances create treed-2 --replica-type=read --region=us-west1
ERROR: (gcloud.sql.instances.create) argument --replica-type: Invalid choice: 'read'.
Valid choices are [FAILOVER, READ].
```

```
[root@b7e118e9200e /]# gcloud sql instances create treed-2  --region=us-west1
Creating Cloud SQL instance...done.                                                                                       
Created [https://sqladmin.googleapis.com/sql/v1beta4/projects/challenge-treed/instances/treed-2].
NAME     DATABASE_VERSION  LOCATION    TIER              PRIMARY_ADDRESS  PRIVATE_ADDRESS  STATUS
treed-2  MYSQL_5_7         us-west1-a  db-n1-standard-1  34.105.64.114    -                RUNNABLE
```


```
[root@b7e118e9200e /]# gcloud sql instances list
NAME     DATABASE_VERSION  LOCATION       TIER              PRIMARY_ADDRESS  PRIVATE_ADDRESS  STATUS
treed    MYSQL_5_7         us-central1-a  db-n1-standard-1  34.69.232.82     -                RUNNABLE
treed-1  MYSQL_5_7         us-east1-b     db-n1-standard-1  34.75.50.152     -                RUNNABLE
treed-2  MYSQL_5_7         us-west1-a     db-n1-standard-1  34.105.64.114    -                RUNNABLE
```

Deploy a stock database of your choice so that it is reachable at the network layer from the
web server. There is no need to have it actually access the database, load data, or design a
schema.
• Write a brief (no more than 1/2 page) set of guidelines to give to developers on how they
should store passwords.

```
There should be a vault cluster deployed. This would allow the the username/password to be managed by the application.
You could then set a one time pasword or give prevent the users.
Consul should be used so you can restrict what application can interact with  has access to the backend.

* Here is an example https://www.consul.io/docs/commands/intention
```

• Write a brief (no more than 1 page) description of the attack surface of the database and
possible mitigations.
#### Here are some security concerns with database

- database permissions from the application perspective
- database permissions from a human perspective
- locking down the operating system
- storing access to the database in the database
- monitoring and setup of the database (4 master cluster or leaderless database cluster)
- database encryption and traffic ecryption
- database upgrade and patching

##### Remedies of attack surface

- placing vault in place for the ability to store password and talk to the database
- placing consul on the services and using vault intentions
- traffic encryption
- setup of database to allow for failure and updates



#### Briefly justify (2-4 points) the choice of database compared to other paradigms. That is, if
you went with Postgres, explain why this was a better choice than MongoDB (NoSQL) or
CockroachDB (NewSQL).


#### Briefly discuss a backup strategy
For postgres you can use tablespace mapping for backing up and restoring. The pg_basebackup takes a backup and tar can be used to compress the backup. You can then store this in an encrypted object store.
Please see here for further explanations
https://www.percona.com/blog/2018/12/21/backup-restore-postgresql-cluster-multiple-tablespaces-using-pg_basebackup/


#### Write a brief (1-2 paragraphs) description of how you would direct reads to the read replica,
but still ensure writes go to the write master.

In this case you are referring to Asynchronous Replication. A multileader cluster setup is an option just not best in one datacenter.
Since we are using gcp with can be in different zones. 
More information is really needed on what type of data and performance we are looking to gain.

I would follow and read up starting with this book https://www.amazon.com/Designing-Data-Intensive-Applications-Reliable-Maintainable/dp/1449373321

https://cloud.google.com/sql/docs/postgres/replication/cross-region-replicas
